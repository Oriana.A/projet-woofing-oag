USE `Project_Woofing_OAG_2`;

DELIMITER $$

USE `Project_Woofing_OAG_2`$$
DROP TRIGGER IF EXISTS `Project_Woofing_OAG`.`User_Stay_AFTER_INSERT_Update_User_Skill` $$
DELIMITER ;
USE `Project_Woofing_OAG`;

DELIMITER $$

DROP TRIGGER IF EXISTS Project_Woofing_OAG.User_Stay_AFTER_INSERT_InsertInto_User_Skill$$
USE `Project_Woofing_OAG`$$
CREATE DEFINER=`simplon`@`localhost` TRIGGER `Project_Woofing_OAG`.`User_Stay_AFTER_INSERT_InsertInto_User_Skill` AFTER INSERT ON `User_Stay` FOR EACH ROW
BEGIN

INSERT INTO User_Skill(Skill_Id, User_Id)
Value (new.Activity_Id, new.User_Id);
END$$
DELIMITER ;