/*Procédure qui permet de calculer le nombre de place restante pour un projet*/

USE Project_Woofing_OAG;
DROP procedure IF EXISTS Remaining_Place_By_Host_Place;

DELIMITER $$
USE Project_Woofing_OAG$$
CREATE DEFINER=`root`@`localhost` PROCEDURE  Remaining_Place_By_Host_Place(IN Id_Host_Place INT)

BEGIN

DECLARE total_volonteer INT;
DECLARE total_capacity INT;

SELECT Hosting_Capacity INTO total_capacity FROM Host_Place WHERE Host_Place.Id = Id_Host_Place;
SELECT COUNT(User_Id) INTO total_volonteer FROM User_Host_Place 
	INNER JOIN User ON User.Id = User_Host_Place.User_Id
	WHERE User_Host_Place.Host_Place_Id = Id_Host_Place AND User.Role_Id = 2;

SELECT total_capacity - total_volonteer AS spot_left;

END$$

DELIMITER ;
