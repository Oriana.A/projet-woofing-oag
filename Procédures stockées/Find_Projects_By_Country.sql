/*Procédure qui permet de trouver des projets par pays*/

USE Project_Woofing_OAG;
DROP procedure IF EXISTS Find_Project_By_Country;

DELIMITER $$
USE Project_Woofing_OAG$$
CREATE DEFINER=`root`@`localhost` PROCEDURE Find_Projects_By_Country(IN From_Country VARCHAR(15))
BEGIN
SELECT Project_Type, Address, Zip_Code, City, Country FROM Host_Place_Project
	LEFT JOIN Project ON Project.Id = Host_Place_Project.Project_Id
    LEFT JOIN Host_Place ON Host_Place.Id = Host_Place_Project.Host_Place_Id
    WHERE Country = From_Country;
END$$

DELIMITER ;
