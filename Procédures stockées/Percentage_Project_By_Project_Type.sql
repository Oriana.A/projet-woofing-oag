USE Project_Woofing_OAG;
DROP procedure IF EXISTS Percentage_Project_By_Project_Type;

DELIMITER $$
USE Project_Woofing_OAG$$
CREATE DEFINER=`root`@`localhost` PROCEDURE Percentage_Project_By_Project_Type(IN Type VARCHAR(25))

BEGIN

DECLARE total_project INT;
DECLARE nbr_project_by_type INT;

SELECT COUNT(Id) INTO total_project from Project;
SELECT COUNT(Id) INTO nbr_project_by_type from Project WHERE Project_Type = Type;

SELECT nbr_project_by_type * 100 / total_project AS Percentage_Project_By_Project_Type;

END$$

DELIMITER ;
