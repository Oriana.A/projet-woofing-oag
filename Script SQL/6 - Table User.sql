/*CREATION DE LA TABLE USER*/
DROP TABLE IF EXISTS User ;
CREATE TABLE IF NOT EXISTS User
(
	Id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Role_Id INT NOT NULL,
    State_Id INT,
    Host_Place_Id INT,
    First_Name VARCHAR(60) NOT NULL,
    Last_Name VARCHAR(60) NOT NULL,
    Email VARCHAR(190) NOT NULL UNIQUE,
	Password VARCHAR(190) NOT NULL,
	UNIQUE INDEX Email_UNIQUE (Email),
	CONSTRAINT FK_State_Id
		FOREIGN KEY (State_Id)
		REFERENCES State(Id)
			ON DELETE CASCADE,
	CONSTRAINT FK_Role_Id
		FOREIGN KEY (Role_Id)
		REFERENCES Role(Id)
			ON DELETE CASCADE,
    CONSTRAINT FK_User_Host_Place_Id
		FOREIGN KEY (Host_Place_Id)
		REFERENCES Host_Place(Id)
			ON DELETE CASCADE       
);
