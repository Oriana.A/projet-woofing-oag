/*ROLE*/
INSERT INTO Role (Id, Role_Label)
	VALUES
	(1, 'Host'),
	(2, 'Volonteer');

/*STATE*/
INSERT INTO State (Id, State_Label)
	VALUES
	(1, 'Registered'),
	(2, 'Accepted'),
    (3, 'Refused'),
    (4, 'Present'),
    (5, 'Absent');

/*USER*/
INSERT INTO User (
    Id,
    Role_Id,
    State_Id,
    First_Name,
    Last_Name,
    Email,
    Password
    )
VALUES
    (1,1,1,"Doug","Daddow","ddaddow0@cnbc.com","nytMvsoYYr"),
    (2,2,1,"Nestor","Bejos", "nridge1@twitter.com", "9oynCtgxF0"),
    (3,2,2,"Sigfrid","Loyd", "sruppel2@nih.gov", "DopTYIF9"),
    (4,1,2,"Katie","Germann", "kgermann3@amazon.de", "L4bnhMef0"),
    (5,2,3,"Bryana","Thompson", "bthompson5@vinaora.com", "sIMms7QIOl6O"),
    (6,1,4,"Luisa","McCloy","lmccloy6@ucsd.edu", "gZgdAPRYaw"),
    (7,2,4,"Demetrius", "Langlois", "dlangloisf@163.com", "2zy2ign99NgF"),
    (8,2,2, "Serena", "Levitt", "slevittg@netscape.com", "lukxL3v8"),
    (9,1,4,"Arvy", "Augustus", "aaugustusj@biblegateway.com", "eEt3n9A31S"),
    (10,1,4,"Bastian","Lapham", "blaphamt@ow.ly", "8l7HdRoE"),
    (11,2,5,"Letti","Olliar", "lolliarn@yellowpages.com", "ImIWogrRqq"),
    (12,2,4,"Idel", "Lauret", "ilauretk@tumblr.com", "c8OTMB4QW"),
    (13,2,1, "Kacey", "Drain", "kdrainm@baidu.com", "J34Knws"),
    (14,2,2, "Simmonds", "Rubs", "srubesl@china.com.cn", "YMVbNA"),
    (15,2,4, "Reinwald","D'Antuoni", "rdantuonic@nba.com", "Sph3SNZT6f"),
    (16,2,4,"Tobe", "Bosman", "tbrosemand@wikia.com", "B1yP8rFgUOb"),
    (17,1,4,"Jerome","Dupont", "jpoker8@symantec.com", "cwrDUv"),
    (18,2,4, "Coretta", "Crohan","ccrohan9@google.ru", "6LaxETe"),
    (19,2,4, "Welbie","Newbie", "weidelman4@discovery.com", "4r1Jv5LXGeSx"),
    (20,1,2, "Vilhelmina","Cullingworth", "vcullingwortho@wikia.com", "965rT6Bl");
       
/*Host_Place*/
INSERT INTO Host_Place (Id, Hosting_Capacity, Address, Country, City, Zip_Code, Type)
	VALUES
    (1,5, "43 Mcbride Alley", "Sweden","Vellinge", "235 36", "chambre simple"),
    (2,10,"4762 Waxwing Hill", "Ivory Coast","Abengourou","0298","dortoir"),
    (3,8,"23 Superior Crossing","Indonesia","Mataloko","A3469","caravane"),
    (4,20,"17092 Laurel Plaza", "Poland","Mirów","53-402","tente"),
    (5,3,"50542 Merry Place", "Oman","Nizwá","24-3514","chambre double"),
    (6,6,"43 Briar Crest Center", "Mozambique","ZP600","Chimoio","dortoir"),
    (7,2, "7157 Alpine Hill","Indonesia","Padangtiji","J76543","chambre simple");

/*Skill*/
INSERT INTO Skill (Id, Label, Description)
	VALUES
    (1,"Recolte miel", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris cursus ut urna nec tristique."),
    (2,"Bêchage", "Aliquam tincidunt mi non feugiat fringilla. Praesent tempus interdum purus nec viverra."),
    (3,"Algoculture","Etiam posuere tortor id erat tempor tristique. "),
    (4,"Maçonnerie",NULL),
    (5,"Cueillete sauvage", "Aenean non eleifend lorem. Nulla ultricies, tortor non posuere consectetur"),
    (6,"Menuiserie", "Nulla ultricies, non eleifend lorem"),
    (7, "Nettoyage", NULL),
    (8,"Observation", NULL),
    (9,"Soin","Lorem ipsum dolor sit amet"),
    (10,"Brassage","erat tempor tristique.");


/*Stay*/
INSERT INTO Stay (Id,Arrival_Date, Departure_Date)
	VALUES
    (1,"2021-07-01","2021-08-01"),
    (2,"2021-01-14","2021-02-06"),
    (3,"2021-02-05","2021-02-19"),
    (4,"2021-03-10","2021-03-28"),
    (5,"2021-06-09","2021-06-21"),
    (6,"2021-07-07","2021-07-25"),
    (7,"2021-08-12","2021-08-22"),
    (8,"2021-01-26","2021-03-26"),
    (9,"2021-02-01","2021-02-28"),
    (10,"2021-06-01","2021-06-10"),
    (11,"2021-06-05","2021-06-15"),
    (12,"2021-02-15","2021-02-15"),
    (13,"2021-01-01","2021-01-15");
    
INSERT INTO Project (Id, Start_Date, End_Date, Description, Project_Type)
	VALUES
    (1,"2021-07-01", "2021-09-01","In eu mattis massa. Cras eget accumsan ante, a viverra sapien.", "Eco-construction"),
    (2,"2021-01-01", "2021-02-15","Nunc vel neque ullamcorper, cursus sem nec, interdum libero.", "Aquaculture"),
    (3,"2021-02-01","2021-02-28","Aenean condimentum ante ut sapien tempus, quis aliquam justo mattis.", "Foresterie"),
    (4,"2021-03-01","2021-03-31","quam velit massa, ultricies sit amet ornare eu, tristique elementum odio.", "Apiculture"),
    (5,"2021-06-01", "2021-06-21","Vestibulum auctor laoreet metus, ac rutrum metus sollicitudin quis.", "Maraîchage"),    
    (6,"2021-07-07","2021-07-25","Nulla facilisi. Vivamus varius et turpis nec sodales.", "Brasserie"),
    (7,"2021-08-10","2021-08-31","Quisque est ipsum, tempus ut magna ac, elementum commodo felis.", "Elevage"),
	(8,"2021-07-01","2021-09-01",NULL,"Eco-construction"),
	(9,"2021-01-01", "2021-02-15",NULL,"Foresterie"),
    (10,"2021-03-01","2021-03-31",NULL,"Apiculture"),
    (11,"2021-03-01","2021-03-31",NULL,"Brasserie"),
    (12,"2021-07-07","2021-07-25",NULL,"Maraîchage"),
    (13,"2021-08-10","2021-08-31",NULL,"Eco-construction"),
    (14,"2021-01-01", "2021-02-15",NULL,"Eco-construction"),
    (15,"2021-07-01","2021-09-01",NULL,"Apiculture");
    
/*TABLES DE JOINTURES*/
/*Host_Place_Project*/
INSERT INTO Host_Place_Project (Host_Place_Id, Project_Id)
	VALUES
	(1, 1),
	(2, 5),
    (3, 2),
    (4, 7),
    (5, 4),
    (6, 1),
    (7, 5),
    (1, 3),
    (5, 6),
    (7, 7);
    
/*Project_Skill*/    
    INSERT INTO Project_Activity (Activity_Id, Project_Id)
	VALUES
	(1,4),
	(2,5),
    (3,2),
    (4,1),
    (5,3),
    (6,1),
    (7,1),
    (8,3),
    (9,7),
    (10,6),
    (8,7);

/*User_Project*/
    INSERT INTO User_Project (User_Id, Project_Id)
	VALUES
	(1,1),
    (1,3),
	(4,2),
    (6,6),
    (6,4),
    (9,5),
    (9,7),
    (10,7),
    (17,1),
    (17,7),
    (17,5),
    (20,2),
    (2,1),
    (3,2),
    (5,3),
    (7,4),
    (8,5),
    (11,6),
    (12,7),
    (13,7),
    (14,6),
    (16,5),
    (18,4),
    (19,3),
    (15,2),
    (2,4),
    (12,1),
    (12,5);

/*User_Skill*/
    INSERT INTO User_Skill (Skill_Id, User_Id)
	VALUES
    (1,3),
    (1,3),
    (2,4),
    (3,5),
    (4,5),
    (5,6),
    (6,6),
    (7,6),
    (8,9),
    (9,9),
    (10,10),
    (2,17),
    (4,17),
    (6,17),
    (10,20);
    
/*User_Stay*/
    INSERT INTO User_Stay (User_Id, Stay_Id, Host_Place_Id, Activity_Id, Project_Id)
	VALUES
    (2,3,1,1,3),
    (3,3,2,2,9),
    (5,4,3,3,4),
    (7,5,4,4,5),
    (8,5,5,6,1),
    (11,6,6,5,6),
    (12,6,7,7,2),
    (13,6,1,2,3),
    (14,9,2,2,4),
    (15,9,3,7,6),
    (16,10,4,3,1),
    (18,5,5,5,7),
    (19,1,6,1,1),
    (6,3,7,3,12),
    (10,13,1,2,2),
    (2,1,2,7,6),
    (19,7,3,5,5),
    (12,8,4,7,2),
    (12,11,5,2,3),
    (14,12,6,7,7); 
    
/*User_Host_Place*/
	INSERT INTO User_Host_Place(User_Id, Host_Place_Id)
    VALUES
	(1,1),
    (4,2),
    (6,3),
    (9,4),
    (10,5),
    (17,6),
    (20,7),
    (2,6),
	(3,4),
    (5,3),
    (7,2),
    (8,5),
    (11,1),
    (12,3),
    (13,7),
    (14,1),
    (15,7),
    (16,3),
    (18,2);
   


