/*TABLE DE JOINTURE USER - Stay*/
CREATE TABLE User_Stay
(
   Stay_Id INT NOT NULL,
   User_Id INT NOT NULL,
   Host_Place_Id INT NOT NULL,
   Activity_Id INT NOT NULL,
   Project_Id INT NOT NULL,
    CONSTRAINT FK_UserStay_Stay_Id__Stay
		FOREIGN KEY (Stay_Id)
		REFERENCES Stay (Id),
	CONSTRAINT FK_UserStay_User_Id__User
		FOREIGN KEY (User_Id)
		REFERENCES User (Id),
	CONSTRAINT FK_UserStay_Project_Id__Project
		FOREIGN KEY (User_Id)
		REFERENCES User (Id),
	CONSTRAINT FK_UserStay_Activity_Id__Skill
		FOREIGN KEY (User_Id)
		REFERENCES User (Id)
);


/*TABLE DE JOINTURE HOST_PLACE - PROJECT*/
CREATE TABLE Host_Place_Project
(
    Host_Place_Id INT NOT NULL,
    Project_Id INT NOT NULL,
    CONSTRAINT FK_HostPlaceProject_Host_Place_Id
		FOREIGN KEY (Host_Place_Id)
		REFERENCES Host_Place (Id),
	CONSTRAINT FK_HostPlaceProject_Project_Id
		FOREIGN KEY (Project_Id)
		REFERENCES Project (Id)
);

/*TABLE DE JOINTURE PROJECT - SKILL*/
CREATE TABLE Project_Activity
(
    Activity_Id INT NOT NULL,
    Project_Id INT NOT NULL,
    CONSTRAINT FK_ProjectSkill_Activity_Id__Skill
		FOREIGN KEY (Activity_Id)
		REFERENCES Skill (Id),
	CONSTRAINT FK_ProjectSkill_Project_Id__Project
		FOREIGN KEY (Project_Id)
		REFERENCES Project (Id)
);

/*TABLE DE JOINTURE USER - SKILL*/
CREATE TABLE User_Skill
(
    Skill_Id INT NOT NULL,
    User_Id INT NOT NULL,
    CONSTRAINT FK_UserSkill_Skill_Id
		FOREIGN KEY (Skill_Id)
		REFERENCES Skill (Id),
	CONSTRAINT FK_UserSkill_User_Id
		FOREIGN KEY (User_Id)
		REFERENCES User (Id)
);

/*TABLE DE JOINTURE USER - Project*/
CREATE TABLE User_Project
(
    User_Id INT NOT NULL,
    Host_Place_Id INT NOT NULL,
    CONSTRAINT FK_UserHostPlace_User_Id
		FOREIGN KEY (User_Id)
		REFERENCES User (Id),
	CONSTRAINT FK_UserHostPlace_Host_Place_Id
		FOREIGN KEY (Host_Place_Id)
		REFERENCES Host_Place (Id)
);


/*TABLE DE JOINTURE USER - HOST_PLACE*/
CREATE TABLE User_Host_Place
(
    User_Id INT NOT NULL,
    Host_Place_Id INT NOT NULL,
    CONSTRAINT FK_UserHostPlace_User_Id
		FOREIGN KEY (User_Id)
		REFERENCES User (Id),
	CONSTRAINT FK_UserHostPlace_Host_Place_Id
		FOREIGN KEY (Host_Place_Id)
		REFERENCES Host_Place (Id)
);