/*CREATION DE LA BDD*/
CREATE DATABASE Project_Woofing_OAG;
USE Project_Woofing_OAG;

/*CREATION DE LA TABLE Host_Place*/
DROP TABLE IF EXISTS Host_Place;
CREATE TABLE IF NOT EXISTS Host_Place
(
	Id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Hosting_Capacity INT NOT NULL,
    Address VARCHAR(150),
    Country VARCHAR(45),
    City VARCHAR(60),
    Zip_Code VARCHAR(15),
    Type VARCHAR (45) NOT NULL,
    INDEX IDX_Country (Country),
    INDEX IDX_City (City),
    INDEX IDX_Capacity (Hosting_Capacity)
);
